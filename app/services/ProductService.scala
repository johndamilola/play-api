package services

import models.{Product, Products}

object ProductService {

    def addProduct(product: Product): Seq[Product] =
        Products.addProduct(product)

    def getProduct(id: Long): Option[Product] =
        Products.getProduct(id)

    def getAllProducts: Seq[Product] =
        Products.getAllProducts

    def deleteProduct(id: Long) =
        Products.deleteProduct(id)

}
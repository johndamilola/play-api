package models

import play.api.Play
import play.api.data.Form
import play.api.data.Forms._

case class Product(id: Long, name: String, price: Double)

case class ProductFormData(name: String, price: Int)

object ProductForm {
    val form = Form(
        mapping(
            "name" -> nonEmptyText,
            "price" -> number
        )(ProductFormData.apply)(ProductFormData.unapply)
    )
}

object Products {

    val products: Seq[Product] = Seq(
        Product(1, "Tecno Camon", 25000),
        Product(2, "Tecno Camon", 25000),
        Product(3, "Tecno Camon", 25000),
        Product(4, "Tecno Camon", 25000)
    )

    def addProduct(product: Product): Seq[Product] =
        products :+ product.copy(id = products.length)

    def getAllProducts: Seq[Product] = 
        products.sortBy(_.id)

    def getProduct(id: Long): Option[Product] = 
        products.find(_.id == id)

    def deleteProduct(id: Long) = 
        products.find(_.id != id)
}
package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json._
import play.api.libs.json.Json
import services.ProductService
import models.{Product, ProductForm}


class ProductController extends Controller {

    implicit val productWrites = Json.writes[Product]

    def index = Action { implicit request =>
        val products = ProductService.getAllProducts
        val result = Json.toJson(products)
        Ok(result)
    }

    def getAddProduct = Action { implicit request =>
        val addRequest = ProductForm.bindFromRequest.get
        Ok(addRequest)
    }

    def singleProduct(id: Long) = Action { implicit request =>
        val product = ProductService.getProduct(id)
        val result = Json.toJson(product)
        Ok(result)
    }

}
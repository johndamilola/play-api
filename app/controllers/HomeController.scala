package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json.Json
import play.api.libs.json._
import services.UserService
import models.{User, UserForm}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class HomeController extends Controller {

  def index = Action { implicit request =>
    val users = UserService.listAllUsers
    Ok("JsObject(users)")
    // users match {
    //   case None => Ok("Nothing Here!")
    //   case _ => Ok("Okay")
    // }
  }

  def addUser() = TODO
  
}
